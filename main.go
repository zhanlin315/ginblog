package main

import (
	"ginblog/models"
	"ginblog/routers"
)

func main() {
	models.InitDB()
	routers.InitRouter()
}