package middlerware

import (
	"ginblog/utils"
	"ginblog/utils/errmsg"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
	"time"
)

var (
	JwtKey = []byte(utils.JwtKey)
)

type MyClaims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}

// 生产token
func SetToken(username string) (string, int) {
	expireTime := time.Now().Add(10 * time.Hour)
	SetClaims := MyClaims{
		username,
		jwt.StandardClaims{
			ExpiresAt: expireTime.Unix(),
			Issuer:    "ginblog",
		},
	}

	reqClaim := jwt.NewWithClaims(jwt.SigningMethodHS256, SetClaims)
	token, err := reqClaim.SignedString(JwtKey)
	if err != nil {
		return "", errmsg.SET_TOKEN_FAILED
	}
	return token, errmsg.SET_TOKEN_SUCCESS
}

// 验证token
func CheckToken(tokenString string) (*MyClaims, int) {
	token, _ := jwt.ParseWithClaims(tokenString, &MyClaims{}, func(token *jwt.Token) (interface{}, error) {
		return JwtKey, nil
	})

	if claims, _ := token.Claims.(*MyClaims); token.Valid {
		return claims, errmsg.SUCCESS
	} else {
		return nil, errmsg.ERROR
	}
}

// jwt中间件
func JwtToken() gin.HandlerFunc {
	return func(c *gin.Context) {
		tokenHeader := c.Request.Header.Get("Authorization")
		var code int
		if tokenHeader == "" {
			code = errmsg.ERROR_TOKEN_NOT_EXIST
			c.JSON(http.StatusOK, gin.H{
				"code":    code,
				"message": errmsg.GetErrMsg(code),
			})
			c.Abort()
			return
		}
		tmp := strings.SplitN(tokenHeader, " ", 2)
		if len(tmp) != 2 && tmp[0] != "Bearer" {
			code = errmsg.ERROR_TOKEN_FMT
			c.JSON(http.StatusOK, gin.H{
				"code":    code,
				"message": errmsg.GetErrMsg(code),
			})
			c.Abort()
			return
		}

		claims, Tcode := CheckToken(tmp[1])
		if Tcode == errmsg.ERROR {
			code = errmsg.ERROR_TOKEN_WRONG
			c.Abort()
		} else if time.Now().Unix() > claims.ExpiresAt {
			code = errmsg.ERROR_TOKEN_EXPIRE
			c.Abort()
		} else {
			c.Set("username", claims.Username)
			c.Next()
		}
	}
}
