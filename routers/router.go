package routers

import (
	v1 "ginblog/api/v1"
	"ginblog/middlerware"
	"ginblog/utils"
	"github.com/gin-gonic/gin"
)

func InitRouter() {
	gin.SetMode(utils.AppMode)
	r := gin.New()
	r.Use(middlerware.Logger())
	r.Use(middlerware.Cors())
	r.Use(gin.Recovery())

	auth := r.Group("api/v1")
	auth.Use(middlerware.JwtToken())
	{
		// 用户模块的路由接口
		//auth.POST("user/add", v1.AddUser)
		auth.PUT("user/:uid", v1.EditUser)
		auth.DELETE("user/:uid", v1.DelUser)
		// 分类模块的路由接口
		auth.POST("cate/add", v1.AddCate)
		auth.PUT("cate/:cid", v1.EditCate)
		auth.DELETE("cate/:cid", v1.DelCate) // 文章模块的路由接口
		auth.POST("article/add", v1.AddArticle)
		auth.PUT("article/:cid", v1.EditArticle)
		auth.DELETE("article/:cid", v1.DelArticle)
		auth.POST("upload", v1.Upload)

	}

	router := r.Group("api/v1")
	{
		router.GET("user", v1.GetUser)
		router.GET("users", v1.GetUsers)
		router.GET("cate", v1.GetCate)
		router.GET("cates", v1.GetCates)
		router.GET("article", v1.GetArticle)
		router.GET("article/category", v1.GetArticleByCate)
		router.GET("articles", v1.GetArticles)
		router.POST("login", v1.Loin)
		router.POST("user/add", v1.AddUser)
	}
	r.Run(utils.HttpPort)
}
