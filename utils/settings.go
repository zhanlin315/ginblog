package utils

import (
	"fmt"
	"gopkg.in/ini.v1"
)

var (
	AppMode  string
	HttpPort string
	JwtKey   string

	DB       string
	DBHost   string
	DBPort   string
	DBUser   string
	DBPasswd string
	DBName   string

	AccessKey string
	SecretKey string
	Bucket    string
	Url       string

	Name      string
	Day       int
	MaxBackup int
	MaxSize   int
	Level     string
)

func init() {
	file, err := ini.Load("config/config.ini")
	if err != nil {
		fmt.Printf("加载配置文件失败，%v\n", err)
		panic(err)
	}
	LoadServer(file)
	LoadDB(file)
	LoadOss(file)
	LoadLog(file)
}

func LoadServer(file *ini.File) {
	AppMode = file.Section("server").Key("AppMode").MustString("debug")
	HttpPort = file.Section("server").Key("HttpPort").MustString(":3000")
	JwtKey = file.Section("server").Key("JwtKey").MustString("")
}

func LoadDB(file *ini.File) {
	DB = file.Section("database").Key("DB").MustString("mysql")
	DBHost = file.Section("database").Key("DBHost").MustString("127.0.0.1")
	DBUser = file.Section("database").Key("DBUser").MustString("ginblog")
	DBPasswd = file.Section("database").Key("DBPasswd").MustString("ginblog")
	DBPort = file.Section("database").Key("DBPort").MustString("3306")
	DBName = file.Section("database").Key("DBName").MustString("ginblog")
}

func LoadOss(file *ini.File) {
	AccessKey = file.Section("oss").Key("AccessKey").String()
	SecretKey = file.Section("oss").Key("SecretKey").String()
	Bucket = file.Section("oss").Key("Bucket").MustString("zlginblog")
	Url = file.Section("oss").Key("Url").MustString("http://qfag3ga3q.hn-bkt.clouddn.com/")
}

func LoadLog(file *ini.File) {
	Name = file.Section("log").Key("Name").MustString("ginblog.log")
	Day = file.Section("log").Key("Day").MustInt(7)
	MaxBackup = file.Section("log").Key("MaxBackup").MustInt(7)
	MaxSize = file.Section("log").Key("MaxSize").MustInt(1024)
	Level = file.Section("log").Key("Level").MustString("debug")
}
