package errmsg

const (
	SUCCESS = 200
	ERROR   = 500

	// CODE = 1000...用户模块的错误
	ADD_USER_SUCESS           = 1000
	ERROR_USERNAME_USED       = 1001
	ERROR_PASSWORD_WRONG      = 1002
	ERROR_USER_NOT_EXIT       = 1003
	ERROR_TOKEN_EXIST         = 1004
	ERROR_TOKEN_NOT_EXIST     = 1005
	ERROR_TOKEN_EXPIRE        = 1006
	ERROR_TOKEN_WRONG         = 1007
	ERROR_TOKEN_FMT           = 1008
	ERROR_USER_UPDATE_FAILED  = 1009
	ERROR_USER_UPDATE_SUCCESS = 1010
	ERROR_USER_DELETE_FAILED  = 1011
	ERROR_USER_DELETE_SUCCESS = 1012
	// CODE = 2000...分类模块的错误
	ADD_CATE_SUCCESS    = 2000
	ADD_CATE_FAILED     = 2001
	EDIT_CATE_SUCESS    = 2002
	EDIT_CATE_FAILED    = 2003
	SELECT_CATE_SUCCESS = 2004
	SELECT_CATE_FALIED  = 2005
	DEL_CATE_SUCCESS    = 2006
	DEL_CATE_FAILED     = 2007
	CATE_EXIST          = 2008
	// CODE = 3000...分类模块的错误
	ADD_ARTICLE_SUCCESS                 = 3000
	ADD_ARTICLE_FAILED                  = 3001
	DEL_ARTICLE_SUCCESS                 = 3002
	DEL_ARTICLE_FAILED                  = 3003
	EDIT_ARTICLE_SUCCESS                = 3004
	EDIT_ARTICLE_FAILED                 = 3005
	SELECT_ARTICLE_SUCCESS              = 3006
	SELECT_ARTICLE_FAILED               = 3007
	SELECT_ARTICLE_LIST_BY_CATE_SUCCESS = 3008
	SELECT_ARTICLE_LIST_BY_CATE_FAILED  = 3009

	// CODE = 6000...属于jwt模块操作
	SET_TOKEN_SUCCESS = 6001
	SET_TOKEN_FAILED  = 6002
)

var codemsg = map[int]string{
	SUCCESS:                             "OK",
	ERROR:                               "内部错误",
	ADD_USER_SUCESS:                     "添加用户成功",
	ERROR_USERNAME_USED:                 "用户名已存在",
	ERROR_PASSWORD_WRONG:                "密码错误",
	ERROR_USER_NOT_EXIT:                 "用户不存在",
	ERROR_TOKEN_EXIST:                   "token已存在",
	ERROR_TOKEN_NOT_EXIST:               "token不存在",
	ERROR_TOKEN_EXPIRE:                  "token已过期",
	ERROR_TOKEN_WRONG:                   "token不正确",
	ERROR_TOKEN_FMT:                     "token格式错误",
	ERROR_USER_UPDATE_FAILED:            "更新用户失败",
	ERROR_USER_UPDATE_SUCCESS:           "更新用户成功",
	ERROR_USER_DELETE_FAILED:            "删除用户失败",
	ERROR_USER_DELETE_SUCCESS:           "删除用户成功",
	ADD_CATE_SUCCESS:                    "添加文章分类成功",
	ADD_CATE_FAILED:                     "添加文章分类失败",
	EDIT_CATE_SUCESS:                    "编辑文章分类成功",
	EDIT_CATE_FAILED:                    "编辑文章分类失败",
	SELECT_CATE_SUCCESS:                 "查询文章分类成功",
	SELECT_CATE_FALIED:                  "查询文章分类失败",
	DEL_CATE_SUCCESS:                    "删除文章成功",
	DEL_CATE_FAILED:                     "删除文章失败",
	CATE_EXIST:                          "文章分类已存在",
	ADD_ARTICLE_SUCCESS:                 "添加文章成功",
	ADD_ARTICLE_FAILED:                  "添加文章失败",
	DEL_ARTICLE_SUCCESS:                 "删除文章成功",
	DEL_ARTICLE_FAILED:                  "删除文章失败",
	EDIT_ARTICLE_SUCCESS:                "编辑文章成功",
	EDIT_ARTICLE_FAILED:                 "编辑文章失败",
	SELECT_ARTICLE_SUCCESS:              "查询文章成功",
	SELECT_ARTICLE_FAILED:               "查询文章失败",
	SELECT_ARTICLE_LIST_BY_CATE_SUCCESS: "根据分类获取文章列表成功",
	SELECT_ARTICLE_LIST_BY_CATE_FAILED:  "根据分类获取文章列表失败",


	SET_TOKEN_SUCCESS: "生产token成功",
	SET_TOKEN_FAILED:  "生产token失败",
}

func GetErrMsg(code int) string {
	return codemsg[code]
}
