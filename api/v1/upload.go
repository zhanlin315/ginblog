package v1

import (
	"ginblog/models"
	"github.com/gin-gonic/gin"
	"net/http"
)

func Upload(c *gin.Context) {
	file, fileHeader, _ := c.Request.FormFile("file")
	fileSize := fileHeader.Size
	url, code := models.UpLoadFile(file, fileSize)
	c.JSON(http.StatusOK,gin.H{
		"code": code,
		"imgUrl": url,
	})
}
