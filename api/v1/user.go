package v1

import (
	"ginblog/models"
	"ginblog/utils/errmsg"
	"ginblog/utils/validator"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// 添加用户
func AddUser(c *gin.Context) {
	var user models.User
	c.ShouldBindJSON(&user)
	msg, code := validator.Validate(&user)
	if code == errmsg.ERROR {
		c.JSON(http.StatusOK, gin.H{
			"code":    code,
			"message": msg,
		})
		return
	}
	code = models.CheckUser(user.Username)
	if code == errmsg.SUCCESS {
		models.CreateUser(&user)
		code = errmsg.ADD_USER_SUCESS
	}
	c.JSON(http.StatusOK, gin.H{
		"code":    code,
		"data":    user,
		"message": errmsg.GetErrMsg(code),
	})
}

// 查询单个用户
func GetUser(c *gin.Context) {
	uid, _ := strconv.Atoi(c.Query("uid"))
	user := models.GetUser(uid)
	code := errmsg.SUCCESS
	if user == nil {
		code = errmsg.ERROR_USER_NOT_EXIT
	}
	c.JSON(http.StatusOK, gin.H{
		"code":    code,
		"data":    user,
		"message": errmsg.GetErrMsg(code),
	})
}

//查询用户列表
func GetUsers(c *gin.Context) {
	pagenum, _ := strconv.Atoi(c.Query("pagenum"))
	pagesize, _ := strconv.Atoi(c.Query("pagesize"))
	if pagenum == 0 {
		pagenum = -1
	}

	if pagesize == 0 {
		pagesize = -1
	}

	users, code, total := models.GetUsers(pagenum, pagesize)
	c.JSON(http.StatusOK, gin.H{
		"code":    code,
		"data":    users,
		"message": errmsg.GetErrMsg(errmsg.SUCCESS),
		"total":   total,
	})
}

//编辑用户
func EditUser(c *gin.Context) {
	var user models.User
	c.ShouldBindJSON(&user)
	uid, _ := strconv.Atoi(c.Param("uid"))
	code := models.EditUser(uid, user.Role)
	c.JSON(http.StatusOK, gin.H{
		"code":    code,
		"message": errmsg.GetErrMsg(code),
	})
}

//删除用户
func DelUser(c *gin.Context) {
	uid, _ := strconv.Atoi(c.Param("uid"))
	code := models.DelUser(uid)
	c.JSON(http.StatusOK, gin.H{
		"code":    code,
		"message": errmsg.GetErrMsg(code),
	})
}
