package v1

import (
	"ginblog/models"
	"ginblog/utils/errmsg"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// 添加文章分类
func AddCate(c *gin.Context) {
	var cate models.Category
	c.ShouldBindJSON(&cate)
	code := models.CheckCate(cate.Name)
	if code == errmsg.SUCCESS {
		code = models.CreateCate(&cate)
	}
	c.JSON(http.StatusOK, gin.H{
		"code":    code,
		"data":    cate,
		"message": errmsg.GetErrMsg(code),
	})
}

// 查询文章分类
func GetCate(c *gin.Context) {
	cid, _ := strconv.Atoi(c.Query("cid"))
	cate := models.GetCate(cid)
	code := errmsg.SUCCESS
	if cate == nil {
		code = errmsg.ERROR_USER_NOT_EXIT
	}
	c.JSON(http.StatusOK, gin.H{
		"code":    code,
		"data":    cate,
		"message": errmsg.GetErrMsg(code),
	})
}

//查询文章分类列表
func GetCates(c *gin.Context) {
	pagenum, _ := strconv.Atoi(c.Query("pagenum"))
	pagesize, _ := strconv.Atoi(c.Query("pagesize"))
	if pagenum == 0 {
		pagenum = -1
	}

	if pagesize == 0 {
		pagesize = -1
	}

	cates, code, total := models.GetCates(pagenum, pagesize)
	c.JSON(http.StatusOK, gin.H{
		"code":    code,
		"data":    cates,
		"message": errmsg.GetErrMsg(errmsg.SUCCESS),
		"total":   total,
	})
}

//编辑文章分类
func EditCate(c *gin.Context) {
	var cate models.Category
	c.ShouldBindJSON(&cate)
	cid, _ := strconv.Atoi(c.Param("cid"))
	code := models.CheckCate(cate.Name)
	if code == errmsg.SUCCESS {
		code = models.EditCate(cid, cate.Name)
	}

	c.JSON(http.StatusOK, gin.H{
		"code":    code,
		"message": errmsg.GetErrMsg(code),
	})
}

//删除文章分类
func DelCate(c *gin.Context) {
	cid, _ := strconv.Atoi(c.Param("cid"))
	code := models.DelCate(cid)
	c.JSON(http.StatusOK, gin.H{
		"code":    code,
		"message": errmsg.GetErrMsg(code),
	})
}
