package v1

import (
	"ginblog/models"
	"ginblog/utils/errmsg"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

// 添加文章
func AddArticle(c *gin.Context) {
	var article models.Article
	c.ShouldBindJSON(&article)
	code := models.CreateArt(&article)
	c.JSON(http.StatusOK, gin.H{
		"code":    code,
		"data":    article,
		"message": errmsg.GetErrMsg(code),
	})
}

// 查询文章
func GetArticle(c *gin.Context) {
	aid, _ := strconv.Atoi(c.Query("aid"))
	article := models.GetArticle(aid)
	code := errmsg.SELECT_ARTICLE_SUCCESS
	if article == nil {
		code = errmsg.SELECT_ARTICLE_FAILED
	}
	c.JSON(http.StatusOK, gin.H{
		"code":    code,
		"data":    article,
		"message": errmsg.GetErrMsg(code),
	})
}

//查询文章列表
func GetArticles(c *gin.Context) {
	pagenum, _ := strconv.Atoi(c.Query("pagenum"))
	pagesize, _ := strconv.Atoi(c.Query("pagesize"))
	if pagenum == 0 {
		pagenum = -1
	}

	if pagesize == 0 {
		pagesize = -1
	}

	articles, code, total := models.GetArts(pagenum, pagesize)
	c.JSON(http.StatusOK, gin.H{
		"code":    code,
		"data":    articles,
		"message": errmsg.GetErrMsg(errmsg.SUCCESS),
		"total":   total,
	})
}

// 根据分类获取文章列表
func GetArticleByCate(c *gin.Context) {
	cid, _ := strconv.Atoi(c.Query("cid"))
	pagenum, _ := strconv.Atoi(c.Query("pagenum"))
	pagesize, _ := strconv.Atoi(c.Query("pagesize"))
	if pagenum == 0 {
		pagenum = -1
	}

	if pagesize == 0 {
		pagesize = -1
	}
	articleList, code, total := models.GetArtsByCate(cid, pagenum, pagesize)
	c.JSON(http.StatusOK, gin.H{
		"code":    code,
		"data":    articleList,
		"message": errmsg.GetErrMsg(errmsg.SUCCESS),
		"total":   total,
	})
}

//编辑文章分类
func EditArticle(c *gin.Context) {
	var article models.Article
	c.ShouldBindJSON(&article)
	aid, _ := strconv.Atoi(c.Param("cid"))
	code := models.EditArt(aid, &article)

	c.JSON(http.StatusOK, gin.H{
		"code":    code,
		"message": errmsg.GetErrMsg(code),
	})
}

//删除文章分类
func DelArticle(c *gin.Context) {
	aid, _ := strconv.Atoi(c.Param("aid"))
	code := models.DelArt(aid)
	c.JSON(http.StatusOK, gin.H{
		"code":    code,
		"message": errmsg.GetErrMsg(code),
	})
}
