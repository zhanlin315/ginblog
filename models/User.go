package models

import (
	"fmt"
	"ginblog/utils/errmsg"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	gorm.Model
	Username string `gorm:"type:varchar(20); not null" json:"username" validate:"required,min=4,max=12" label:"用户名"`
	Password string `gorm:"type:varchar(64); not null" json:"password" validate:"required,min=6,max=20" label:"密码"`
	Role     int    `gorm:"type:int;default:2" json:"role" validate:"required,gte=2" label:"角色"`
}

// 查询用户是否存在
func CheckUser(username string) int {
	var user User
	db.Select("id").Where("username = ?", username).First(&user)
	if user.ID > 0 {
		return errmsg.ERROR_USERNAME_USED
	}
	return errmsg.SUCCESS
}

// 新增用户
func CreateUser(user *User) int {
	err = db.Create(&user).Error
	fmt.Printf("%v\n", err)
	if err != nil {
		return errmsg.ERROR
	}
	return errmsg.SUCCESS
}

// 查询单个用户
func GetUser(uid int) *User {
	var user User
	db.Where("id = ?", uid).Find(&user)
	if user.ID == 0 {
		return nil
	}
	return &user
}

// 查询用户列表
func GetUsers(pagenum, pagesize int) ([]User, int, int) {
	var users []User
	var total int
	err = db.Limit(pagesize).Offset((pagenum - 1) * pagesize).Find(&users).Count(&total).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		return nil, errmsg.ERROR_USER_NOT_EXIT, total
	}
	return users, errmsg.SUCCESS, total
}

func (u *User) BeforeSave() {
	u.Password = GeneratePassword(u.Password)
}

func (u *User) CheckPassword(password string) bool {
	return bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password)) == nil
}

// 密码加密
func GeneratePassword(password string) string {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), 0)
	if err != nil {
		panic(err.Error())
	}
	return string(hash)
}

// 编辑用户
func EditUser(uid, role int) (code int) {
	var user User
	err = db.Model(&user).Where("id = ?", uid).Update("role", role).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		code = errmsg.ERROR_USER_UPDATE_FAILED
	} else {
		code = errmsg.ERROR_USER_UPDATE_SUCCESS
	}
	return code
}

// 删除用户
func DelUser(uid int) (code int) {
	var user User
	err = db.Where("id = ?", uid).Delete(&user).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		code = errmsg.ERROR_USER_DELETE_FAILED
	} else {
		code = errmsg.ERROR_USER_DELETE_SUCCESS
	}
	return code
}

// 验证用户登录
func VerifyUser(username, password string) (*User, int) {
	var user User
	err = db.Where("username = ?", username).First(&user).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		return nil, errmsg.ERROR_USER_NOT_EXIT
	}
	if user.CheckPassword(password) {
		return &user, errmsg.SUCCESS
	}
	return nil, errmsg.ERROR_PASSWORD_WRONG
}
