package models

import (
	"fmt"
	"ginblog/utils/errmsg"
	"github.com/jinzhu/gorm"
)

type Article struct {
	gorm.Model
	Title    string    `gorm:"type:varchar(100);not null" json:"title"`
	Category *Category `gorm:"foreignkey:Cid" json:"category"`
	Cid      int       `gorm:"type:int" json:"cid"`
	Desc     string    `gorm:"type:varchar(100)" json:"desc"`
	Content  string    `gorm:"type:longtext" json:"content"`
	Img      string    `gorm:"type:varchar(100)" json:"img"`
}

// 新增文章
func CreateArt(article *Article) int {
	err = db.Create(article).Error
	fmt.Printf("%v\n", err)
	if err != nil {
		return errmsg.ADD_ARTICLE_FAILED
	}
	return errmsg.ADD_ARTICLE_SUCCESS
}

// 查询单个文章
func GetArticle(cid int) *Article {
	var article Article
	db.Preload("Category").Where("id = ?", cid).Find(&article)
	if article.ID == 0 {
		return nil
	}
	return &article
}

// 查询分类列表
func GetArts(pagenum, pagesize int) ([]Article, int, int) {
	var articles []Article
	var total int
	err = db.Preload("Category").Limit(pagesize).Offset((pagenum - 1) * pagesize).Find(&articles).Count(&total).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		return nil, errmsg.SELECT_ARTICLE_FAILED, total
	}
	return articles, errmsg.SELECT_ARTICLE_SUCCESS, total
}

// 根据分类获取文章列表
func GetArtsByCate(cid, pagenum, pagesize int) ([]Article, int, int) {
	var articleList []Article
	var total int
	err = db.Preload("Category").Limit(pagesize).Offset((pagenum-1)*pagesize).Where("cid = ?", cid).Find(&articleList).Count(&total).Error
	if err != nil {
		return nil, errmsg.SELECT_ARTICLE_LIST_BY_CATE_FAILED, total
	}
	return articleList, errmsg.SELECT_ARTICLE_LIST_BY_CATE_FAILED, total
}

// 编辑文章
func EditArt(aid int, article *Article) (code int) {
	var updateOptions = make(map[string]interface{})
	updateOptions["Title"] = article.Title
	updateOptions["Cid"] = article.Cid
	updateOptions["Desc"] = article.Desc
	updateOptions["Content"] = article.Content
	updateOptions["Img"] = article.Img
	err = db.Model(&article).Where("id = ?", aid).Update(updateOptions).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		code = errmsg.EDIT_ARTICLE_FAILED
	} else {
		code = errmsg.EDIT_ARTICLE_SUCCESS
	}
	return code
}

// 删除文章
func DelArt(aid int) (code int) {
	var article Article
	err = db.Where("id = ?", aid).Delete(&article).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		code = errmsg.DEL_ARTICLE_FAILED
	} else {
		code = errmsg.DEL_ARTICLE_SUCCESS
	}
	return code
}
