package models

import (
	"fmt"
	"ginblog/utils/errmsg"
	"github.com/jinzhu/gorm"
)

type Category struct {
	ID   uint   `gorm:"primary_key;auto_increment" json:"id"`
	Name string `gorm:"type:varchar(20);not null" json:"name"`
}

// 查询分类是否存在
func CheckCate(name string) int {
	var cate Category
	db.Select("id").Where("name = ?", name).First(&cate)
	if cate.ID > 0 {
		return errmsg.CATE_EXIST
	}
	return errmsg.SUCCESS
}

// 新增文章分类
func CreateCate(cate *Category) int {
	err = db.Create(cate).Error
	fmt.Printf("%v\n", err)
	if err != nil {
		return errmsg.ADD_CATE_FAILED
	}
	return errmsg.ADD_CATE_SUCCESS
}

// 查询单个分类
func GetCate(cid int) *Category {
	var cate Category
	db.Where("id = ?", cid).Find(&cate)
	if cate.ID == 0 {
		return nil
	}
	return &cate
}

// 查询分类列表
func GetCates(pagenum, pagesize int) ([]Category,int,int) {
	var cates []Category
	var total int
	err = db.Limit(pagesize).Offset((pagenum - 1) * pagesize).Find(&cates).Count(&total).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		return nil,errmsg.SELECT_CATE_FALIED,total
	}
	return cates,errmsg.SELECT_CATE_SUCCESS,total
}

// 编辑文章分类
func EditCate(uid int, name string) (code int) {
	var cate Category
	err = db.Model(&cate).Where("id = ?", uid).Update("name", name).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		code = errmsg.EDIT_CATE_FAILED
	} else {
		code = errmsg.EDIT_CATE_SUCESS
	}
	return code
}

// 删除文章分类
func DelCate(cid int) (code int) {
	var cate Category
	err = db.Where("id = ?", cid).Delete(&cate).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		code = errmsg.DEL_CATE_FAILED
	} else {
		code = errmsg.DEL_CATE_SUCCESS
	}
	return code
}
