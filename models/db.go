package models

import (
	"fmt"
	"ginblog/utils"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"time"
)

var (
	db  *gorm.DB
	err error
)

func InitDB() {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		utils.DBUser,
		utils.DBPasswd,
		utils.DBHost,
		utils.DBPort,
		utils.DBName,
	)
	fmt.Println(dsn)
	db, err = gorm.Open(utils.DB, dsn)
	if err != nil {
		panic(err)
	}
	//defer db.Close()

	// 设置连接池中的最大闲置连接数
	db.DB().SetMaxIdleConns(10)
	// 设置数据库的最大连接数
	db.DB().SetMaxOpenConns(100)
	// 设置连接的最大可复用时间
	db.DB().SetConnMaxLifetime(10 * time.Second)
	// 禁用默认表明复数形式
	db.SingularTable(true)
	db.AutoMigrate(&User{}, &Category{}, &Article{})



}
